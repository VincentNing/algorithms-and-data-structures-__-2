﻿using System;
using System.IO;
using System.Diagnostics;

namespace assessment_1_for_algorithms_and_data_structures
{
    class Program
    {
        static public string YorN;//varible for asking user to continue or nah
        static public double[] Max = new double[10];//array for question one to collect top 10 values
        static public double[] NumArr = new double[0];//array for holding the imported Moisture_Data values
        static public string[] DateTime = new string[0];//array for holding the imported DateTime_Data values
        static public int quesNum;//variable for different questions, the program reacts differently by different question
        static public double xMax;//variable used in question2 and question3 to display the maximum value
        static public double xMin;//variable used in question2 and question3 to display the minimum value
        static public double xAva;//variable used in question2 and question3 to display the average value
        static void Main(string[] args)
        {
            do
            {
                Console.Clear();
                Console.WriteLine("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
                Console.WriteLine("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
                Console.WriteLine("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
                Console.WriteLine("$$$$$$$$$$$$                                                            $$$$$$$$$$$$");
                Console.WriteLine("$$$$$$$$$$$$              Welcome to the puzzle of Vincent              $$$$$$$$$$$$");
                Console.WriteLine("$$$$$$$$$$$$                                                            $$$$$$$$$$$$");
                Console.WriteLine("$$$$$$$$$$$$   Choose a number below to implement different programs!   $$$$$$$$$$$$");
                Console.WriteLine("$$$$$$$$$$$$                                                            $$$$$$$$$$$$");
                Console.WriteLine("$$$$$$$$$$$$        1.Finding the top 10 value of a text file           $$$$$$$$$$$$");
                Console.WriteLine("$$$$$$$$$$$$                                                            $$$$$$$$$$$$");
                Console.WriteLine("$$$$$$$$$$$$                      2.Selection sort                      $$$$$$$$$$$$");                 //user interface to welcome
                Console.WriteLine("$$$$$$$$$$$$                                                            $$$$$$$$$$$$");
                Console.WriteLine("$$$$$$$$$$$$                       3.Linear Search                      $$$$$$$$$$$$");
                Console.WriteLine("$$$$$$$$$$$$                                                            $$$$$$$$$$$$");
                Console.WriteLine("$$$$$$$$$$$$                        4.Bubble sort                       $$$$$$$$$$$$");
                Console.WriteLine("$$$$$$$$$$$$                                                            $$$$$$$$$$$$");
                Console.WriteLine("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
                Console.WriteLine("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
                Console.WriteLine("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");

                var read = Console.ReadLine();//readkey to read which question that user choose
                if (read == "1")//if it is the question1, do the following code
                {
                    Console.Clear();
                    importData();//call the import method to import data to work further
                    Console.WriteLine("Top 10 values:\n");
                    findMaximum();//call the findMaximum method to find top 10 values as question1 required
                    quesNum = 1;
                    printArray(Max);//display the result using printArray method
                    Console.WriteLine("Do you want to repeat the program again, 'y' for yes, 'n' for no.");
                    YorN = Console.ReadLine();//asking users if they want to do more with this program

                }
                else if (read == "2")//if it is the question2, do the following code
                {
                    Console.Clear();
                    quesNum = 2;
                    importData();//call the import method to import data to work further
                    Console.WriteLine("Unsorted value:\n");
                    printArray(NumArr);//to display the unsorted arr when it is just finished importing using printArray method as question2 required
                    Console.WriteLine("Click 'Enter' to view sorted Data!");
                    Console.ReadKey();//for the tidiness of the the user interface, to divide it by pressing a key instead just show the all messive data once
                    Console.WriteLine("Sorted value:\n");
                    double[] tempArr = selectionSort();//for the reason to keep the original data, I created another array to do further and used selectionSort method to sorted data
                    printArray(tempArr);//print the sorted data with printArray  method
                    Console.WriteLine("Click 'Enter' to view maximum, minimum and average of the values!");//for the tidiness of the the user interface, to divide it by pressing a key instead just show the all messive data once
                    Console.ReadKey();
                    MaxMinAva();//as the question2 required to find the maximum,the minimum and the avarage value of the data using MaxMinAva method
                    Console.WriteLine();
                    Console.WriteLine("Do you want to repeat the program again, 'y' for yes, 'n' for no.");
                    YorN = Console.ReadLine();//asking users if they want to do more with this program
                }
                else if (read == "3")//if it is the question3, do the following code
                {
                    Console.Clear();
                    quesNum = 3;
                    importData();//call the import method to import data to work further
                    selectionSort();//using selectionSort method to sort the data
                    linearSearch();//using linearSearch method to serach out the required data
                    Console.WriteLine("Click 'Enter' to view what time were maximum, minimum and average of the values occurred!");
                    Console.ReadKey();//for the tidiness of the the user interface, to divide it by pressing a key instead just show the all messive data once
                    compareArrays();//using compareArrays method to find out the data at the same location in another array.
                    Console.WriteLine("\nDo you want to repeat the program again, 'y' for yes, 'n' for no.");
                    YorN = Console.ReadLine();//asking users if they want to do more with this program
                }
                else if (read == "4")//if it is the question4, do the following code
                {
                    Console.Clear();
                    quesNum = 4;
                    BubbleSortTiming bubbleSorting = new BubbleSortTiming();//as required for question4 to created a nre calss, and called the classs
                    bubbleSorting.bubbleSort();//using bubbleSort method to do a regular bubble sort 
                    bubbleSorting.improvedBubbleSort();// using improvedBubbleSort method to do other 2 improved bubble sort, and compare the improvement.
                    Console.WriteLine("\nDo you want to repeat the program again, 'y' for yes, 'n' for no.");
                    YorN = Console.ReadLine();//asking users if they want to do more with this program
                }
                else
                {
                    Console.WriteLine("Reading error, please insert again!");//if the user input is not the question numbers, dispaly the error meesage to users
                    YorN = "y";
                }
            } while (YorN == "y");

            if (YorN == "n")
            {
                Console.Clear();
                Console.WriteLine("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
                Console.WriteLine("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
                Console.WriteLine("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
                Console.WriteLine("$$$$$$$$$$$$                                                            $$$$$$$$$$$$");
                Console.WriteLine("$$$$$$$$$$$$                Thank you for using our program             $$$$$$$$$$$$");
                Console.WriteLine("$$$$$$$$$$$$                                                            $$$$$$$$$$$$");
                Console.WriteLine("$$$$$$$$$$$$               Have a lovly time with your life             $$$$$$$$$$$$");
                Console.WriteLine("$$$$$$$$$$$$                                                            $$$$$$$$$$$$");
                Console.WriteLine("$$$$$$$$$$$$                We hope to see u next time here             $$$$$$$$$$$$");
                Console.WriteLine("$$$$$$$$$$$$                                                            $$$$$$$$$$$$");       //user interface to fearwell
                Console.WriteLine("$$$$$$$$$$$$                    We were happy having you                $$$$$$$$$$$$");
                Console.WriteLine("$$$$$$$$$$$$                                                            $$$$$$$$$$$$");
                Console.WriteLine("$$$$$$$$$$$$                          GoodBye!!!!                       $$$$$$$$$$$$");
                Console.WriteLine("$$$$$$$$$$$$                                                            $$$$$$$$$$$$");
                Console.WriteLine("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
                Console.WriteLine("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
                Console.WriteLine("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
            }
            else
            {
                Console.WriteLine("Error,reboot the peogram please!");//if the user input is not the machine required, dispaly the error meesage to users
            }
        }
        static public void importData()//method required for importing data
        {
            double temp = 0;//variable for holding the decimal values transfered from string type
            int counter = 0;//variable for making the number to increase when last one is imported 
            string line;//hold the first valur befor thansfer it to decimal value

            NumArr = null;//make sure the array is empty

            StreamReader file = new StreamReader(@"c:\Moisture_Data.txt");//import the file of the data and name it as "file"
            if (quesNum == 3)//if it is the third question, import another file which is only used by question3
            {
                StreamReader file2 = new StreamReader(@"c:\DateTime_Data.txt");//make it called "file2"
                while ((line = file2.ReadLine()) != null)//while loop to read and import the file line by line
                {
                    Array.Resize(ref DateTime, counter + 1);//resize the array everytime if there is more value to add in
                    DateTime[counter] = line;//making sure that next line still have data to import
                    counter++;//increase the counter for next line of value
                }
            }

            counter = 0;//reset the value of counter before doing future
            while ((line = file.ReadLine()) != null)//while loop to read and import the file line by line
            {
                double.TryParse(line, out temp);//convert the string type valur to the decimal type value
                Array.Resize(ref NumArr, counter + 1);//resize the array everytime if there is more value to add in
                NumArr[counter] = temp;
                counter++;//increase the counter for next line of value
            }
            file.Close();//close the file when it is finished importing
        }
        static public void findMaximum()//method required for question1 to find the top 10 values out of the data file
        {
            double[] tempArr = new double[NumArr.Length];
            Array.Copy(NumArr, tempArr, NumArr.Length);//make a copy of the array to work on the question, keep the orginal array value for other questions

            for (int i = 0; i < Max.Length; i++)//loop for finding the top 10 values
            {
                double max = 0; //variable for holding the top value found at each time
                int maxIndex = -1;// variable for locate the top value and set it to 0 for finding the next top value next time
                for (int x = 0; x < tempArr.Length; x++)//loop for find the top value of the file
                {
                    if (tempArr[x] > max)
                    {
                        max = tempArr[x];
                        maxIndex = x;
                    }//max will be the top value;maIndex will be the location of the top value
                }
                Max[i] = max;//import the top value to the top 10 array 
                tempArr[maxIndex] = 0;//set the top value to 0 to find next top
            }
        }
        static public void printArray(double[] arr)//method the required for printing array
        {
            for (int i = 1; i <= arr.Length; i++)
            {
                Console.Write(arr[i - 1] + ",  ");
                if (i % 10 == 0)                        //will only display 10 value per line.
                    Console.WriteLine();
            }

            Console.WriteLine();
        }
        static public double[] selectionSort()//method required for question 2 of selection sort
        {
            double temp;
            int min;
            double[] tempArr = new double[NumArr.Length];
            Array.Copy(NumArr, tempArr, NumArr.Length);//make a copy of the array to work on the question, keep the orginal array value for other questions

            for (int i = 0; i < tempArr.Length - 1; i++)
            {
                min = i;//
                for (int sort = i + 1; sort < tempArr.Length; sort++)
                {                                                    //selection sort
                    if (tempArr[sort] < tempArr[min])                //using loop to find the lowest value everytime, then set it to the most front of the array
                    {                                                //swap value with the first array and do next sorting loop by ignoring the first lowest value
                        min = sort;
                    }
                }
                if (min != 1)//debuging by if it is a real value 
                {
                    temp = tempArr[i];
                    tempArr[i] = tempArr[min];   //use temporary value to hold and swap two values
                    tempArr[min] = temp;
                }
            }
            double avarage = 0;//variable for the aravage value
            for (int i = 0; i < tempArr.Length; i++)//loop for adding all values together for future devide by length to get the avarage value
            {
                avarage += tempArr[i];
            }
            xMax = tempArr[tempArr.Length - 1];//the last sorted value will be the largest value
            xMin = tempArr[0];//the first value of the array will be the minimum value
            xAva = avarage / tempArr.Length;//devide by length to get the avarage value

            return tempArr;
        }
        static public void MaxMinAva()//method to print the Maximum, the Minimum and the Avarage value out from the array for the question2
        {
            
            
            Console.WriteLine("Maximum value:   " + xMax);
            Console.WriteLine("Minimum value:   " + xMin);
            Console.WriteLine("Avarage value:   " + xAva);

        }
        static public void linearSearch()//method required for question 3 of linear search
        {
            Console.WriteLine("Maximum value locates at: "); 
            for (int i = 0; i < NumArr.Length; i++)//find the location of the maximum value located in the unsorted data
            {
                
                if (NumArr[i] == xMax)
                {
                    Console.Write(i + 1 + ",  ");

                }
            }
            Console.WriteLine();
            Console.WriteLine("Minimum  value locates at: ");//find the location of the minimum value located in the unsorted data

            for (int i = 0; i < NumArr.Length; i++)
            {
                if (NumArr[i] == xMin)
                {
                    Console.Write(i + 1 + ",  ");
                }
            }
            Console.WriteLine();
            Console.WriteLine("Avarage value locates at: ");//find the location of the avarage value located in the unsorted data

            for (int i = 0; i < NumArr.Length; i++)
            {
                if (NumArr[i] == xAva)
                {
                    Console.Write(i + 1 + ",  ");                                    //Due to the reason that avarage value is calculated value, so checking if there are some value equal to the avarage value                       
                }                                                                    //display if there is any, show the no value message if there are no values like that
                else if (i==NumArr.Length-1&&NumArr[i]!=xAva)
                {
                    Console.WriteLine("No aviable data can be displayed");
                }
            }
        }
        static public void compareArrays()
         {
            Console.WriteLine("Maximum value was occurred at: ");
            for (int i = 0; i < NumArr.Length; i++)
            {
                if (NumArr[i] == xMax)                                   //finding the data location in another file which is dateTime,and display them                   
                {
                    Console.WriteLine(DateTime[i]);
                }
            }
            Console.WriteLine();
            Console.WriteLine("Minimum value was occurred at: ");
            for (int i = 0; i < NumArr.Length; i++)
            {
                if (NumArr[i] == xMin)                                     //finding the data location in another file which is dateTime,and display them
                {
                    Console.WriteLine(DateTime[i]);
                }
            }
            Console.WriteLine("Avarage value was occurred at: ");
            Console.WriteLine();
            for (int i = 0; i < NumArr.Length; i++)
            {
                if (NumArr[i] == xAva)
                {                                                           //finding the data location in another file which is dateTime,and display them if there is any
                    Console.WriteLine(DateTime[i]);
                }
                else if (i==NumArr.Length-1&&NumArr[i]!=xAva)
                {
                    Console.WriteLine("No aviable data can be displayed");
                }
            }
        }
    }
    class BubbleSortTiming//class required for question 4
    {
        public double temp = 0;//variable for holding the decimal values transfered from string type
        public int counter = 0;//variable for making the number to increase when last one is imported 
        public string line;//hold the first valur befor thansfer it to decimal value
        public double[] arr = new double [0];//array for holding the imported data
        Stopwatch st = new Stopwatch();//timer created for comparing the time of each sorting method 
        public void bubbleSort()//normal bubble sorting method 
        {
            
            StreamReader file = new StreamReader(@"c:\Moisture_Data.txt");
            while ((line = file.ReadLine()) != null)
            {
                double.TryParse(line, out temp);
                Array.Resize(ref arr, counter + 1);
                arr[counter] = temp;
                counter++;
            }                                                               //import data just like the first time do, and store them in to the array "arr"
            file.Close();
            double[] NorArr = new double[arr.Length];//make a copy of the array to work on the question, keep the orginal array value for other questions
            Array.Copy(arr, NorArr, arr.Length);
            

            st.Reset();//reset the timer
            st.Start();//start the timer for the normal bubble sort
            for (int write = 0; write < NorArr.Length; write++)
            {
                for (int sort = 0; sort < NorArr.Length - 1; sort++)
                {                                                                       //normal bubble sort
                    if (NorArr[sort] > NorArr[sort + 1])                                //checking if the value is bigger then next value , if do , swap those two, if not,keep going, untill all sorted
                    {
                        temp = NorArr[sort + 1];
                        NorArr[sort + 1] = NorArr[sort];
                        NorArr[sort] = temp;
                    }
                }
            }
            st.Stop();//stop the timer for the normal bubble sort
            Console.WriteLine("Normal bubble sort takes: {0}", st.Elapsed);//show out the record of the timer
            Console.WriteLine();
        }
        public void improvedBubbleSort()//method required for improved bubble sort
        {
            double[] Imp1Arr = new double[arr.Length];
            Array.Copy(arr, Imp1Arr, arr.Length);//make a copy of the original data array for the first improved bubble sort
            double[] Imp2Arr = new double[arr.Length];
            Array.Copy(arr, Imp2Arr, arr.Length);//make a copy of the original data array for the second improved bubble sort
            
            st.Reset();//reset the timer
            st.Start();//start the timer for the first improved bubble sort
            for (int write = 0; write < Imp1Arr.Length; write++)
            {
                for (int sort = 0; sort < Imp1Arr.Length - 1-write; sort++)
                {
                    if (Imp1Arr[sort] > Imp1Arr[sort + 1])                      //bubble sort improvement 1
                    {                                                           //similar to the noraml bubble sort, the upgrade is at the first loop,if it is sorted at the first position at the array
                        temp = Imp1Arr[sort + 1];                               //removed the first position befor doing second loop, so instead of making 99 comparisons on every pass of a 100 element array
                        Imp1Arr[sort + 1] = Imp1Arr[sort];                      //it will now make 98 comparisons on the second pass, 97 on the third pass and so on.
                        Imp1Arr[sort] = temp;
                    }
                }
            }
            st.Stop();//stop the timer for the first improved bubble sort
            Console.WriteLine("Improved bubble sort 1 takes: {0}", st.Elapsed);//show out the record of the timer
            st.Reset();//reset the timer
            st.Start();//start the timer for the second improved bubble sort
            
            for (int write = 0; write < Imp2Arr.Length; write++)
            {
                int test =0;                                                  //bubble sort improvement 12
                for (int sort = 0; sort < Imp2Arr.Length - 1 - write; sort++) //the upgrade is if there is no changes made, just print out the result instead of doing a few more useless sorting
                {                                                             //to check at the end of each pass whether any swaps have been made. If none have been made, the data must already be in the proper order, so the algorithm should terminate.
                    if (Imp2Arr[sort] > Imp2Arr[sort + 1])                    //created a variable called "test" then set the value equals something,then in the loop change the value, at the end if the value"test" is changed, keep going, if not just break, and show the result!
                    {
                        temp = Imp2Arr[sort + 1];
                        Imp2Arr[sort + 1] = Imp2Arr[sort];
                        Imp2Arr[sort] = temp;
                        test=1;
                    }
                }
                if(test==0)
                {
                    break;
                }

            }
            st.Stop(); //stop the timer for the second improved bubble sort



            // for (int i = 1; i <= Imp1Arr.Length; i++)
            // {
            //     Console.Write(Imp1Arr[i - 1] + ",  ");               //using for printing data to test if it is working.
            //     if (i % 10 == 0)                                     //will only display 10 value per line.
            //         Console.WriteLine();
            // }
            



            Console.WriteLine("\nImproved bubble sort 2 takes: {0}", st.Elapsed);//show out the record of the timer

        }
    }
}
